﻿namespace OSRSClicker
{
    internal class Delimitation
    {
        public int length;
        public int height;
        public Point startPoint;

        public Delimitation(int aLength, int aHeight, Point aStartPosition)
        {
            this.length = aLength;
            this.height = aHeight;
            this.startPoint = aStartPosition;
        }
    }
}