﻿namespace OSRSClicker
{
    internal class Point
    {
        public int x;
        public int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public void ChangeX(int x)
        {
            this.x = x;
        }

        public void ChangeY(int y)
        {
            this.y = y;
        }
    }
}