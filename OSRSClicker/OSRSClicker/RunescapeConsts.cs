﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSRSClicker
{
    static class RunescapeConsts
    {
        static Point FIRST_INVENTORY_ITEM_POINT = new Point(1810, 740);
        static Point HIGH_ALCH_POINT = new Point(1780, 770);
        static int ITEM_TO_ITEM_X_DISTANCE = 90;
        static int ITEM_TO_ITEM_Y_DISTANCE = 40;
    }
}
