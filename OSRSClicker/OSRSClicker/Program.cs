using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSRSClicker
{
    class Program
    {
        static void Main(string[] args)
        {
            RoutineBuilder rtb = new RoutineBuilder();

            rtb.addHighAlchAction();

            Routine routine = rtb.Build();

            while (true)
            {
                routine.StartRoutine();
            }
        }
    }
}