﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OSRSClicker.Actions;

namespace OSRSClicker
{
    class Routine
    {
        List<IAction> actions;

        public Routine()
        {
            this.actions = new List<IAction>();
        }

        public void StartRoutine()
        {
            foreach (IAction a in this.actions)
            {
                a.StartAction();
            }
        }

        public void AddAction(IAction action)
        {
            this.actions.Add(action);
        }

        public void UndoDelimitation()
        {
            this.actions.Remove(this.actions[this.actions.Count() - 1]);
        }
    }
}
