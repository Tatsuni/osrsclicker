﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSRSClicker.Actions
{
    class Wait: IAction
    {
        private int waitTime;

        public Wait(int aWaitTime)
        {
            this.waitTime = aWaitTime;
        }

        public void StartAction()
        {
            System.Threading.Thread.Sleep(this.waitTime);
        }
    }
}
