﻿namespace OSRSClicker
{
    internal interface IAction
    {
        void StartAction();
    }
}