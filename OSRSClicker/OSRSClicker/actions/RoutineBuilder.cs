﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OSRSClicker.Actions;

namespace OSRSClicker
{
    class RoutineBuilder
    {
        List<IAction> actionList;

        public RoutineBuilder()
        {
            this.actionList = new List<IAction>();
        }

        public Routine Build()
        {
            Routine routine = new Routine();
            foreach (IAction a in actionList)
            {
                routine.AddAction(a);
            }

            return routine;
        }

        public void addClickAction(Delimitation del)
        {
            this.actionList.Add(new Click(del));
        }

        public void addWaitAction(int waitTime)
        {
            this.actionList.Add(new Wait(waitTime));
        }

        public void addHighAlchAction()
        {
            this.actionList.Add(new HighAlch());
        }
    }
}
