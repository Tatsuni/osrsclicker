﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSRSClicker.Actions
{
    class HighAlch: IAction
    {
        Click mageBookAlch = new Click(new Delimitation(15, 15, new Point(1810, 740)));
        Click inventoryItem = new Click(new Delimitation(40, 40, new Point(1780, 770)));

        Wait firstWaitTime = new Wait(400);
        Wait secondWaitTime = new Wait(2625);

        public HighAlch()
        {}

        public void StartAction()
        {
            mageBookAlch.StartAction();
            firstWaitTime.StartAction();
            inventoryItem.StartAction();
            secondWaitTime.StartAction();
        }
    }
}
