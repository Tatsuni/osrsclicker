﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSRSClicker.Actions
{
    class Click: IAction
    {
        private Delimitation del;

        public Click(Delimitation aDel)
        {
            this.del = aDel;
        }

        public void StartAction()
        {
            Random r = new Random();
            int xPosition = r.Next(0, this.del.length);
            int yPosition = r.Next(0, this.del.height);

            Mouse.LeftMouseClick(this.del.startPoint.x + xPosition, this.del.startPoint.y + yPosition);
        }
    }
}
